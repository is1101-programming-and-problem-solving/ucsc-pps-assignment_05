//SALU Dissanayake - 202029
//IS1101 Programming and Problem Solving
//Assignment 05
/*Imagine the owner of a movie theater who has complete freedom in setting ticket prices.
The more he charges, the fewer the people who can afford tickets.
In a recent experiment the owner determined a precise relationship between the price of a ticket and average attendance.
At a price of Rs 15.00 per ticket, 120 people attend a performance.
Decreasing the price by 5 Rupees increases attendance by 20 and increasing the price by 5 Rupees decreases attendance by 20.
Unfortunately, the increased attendance also comes at an increased cost.
Every performance costs the owner Rs 500.00 Each attendee costs another 3 Rupees.
The owner would like to know the exact relationship between profit and ticket price.
So that he can determine the price at which he can make the highest profit.*/


#include <stdio.h>

int attendance(float ticketPrice);
float income(float ticketPrice);
float cost(float ticketPrice);
float profit(float ticketPrice);

int main () {
    float ticketPrice;
    printf ("Enter ticket price: ");
    scanf ("%f",&ticketPrice);
    printf ("\nProfit is Rs.%.2f\n",profit(ticketPrice));
    return 0;
}

int attendance(float ticketPrice) {
    return 120-(ticketPrice-15)*4;
}

float income(float ticketPrice) {
    return ticketPrice*attendance(ticketPrice);
}

float cost(float ticketPrice) {
    return 500+3*attendance(ticketPrice);
}

float profit(float ticketPrice) {
    return income(ticketPrice)-cost(ticketPrice);
}
