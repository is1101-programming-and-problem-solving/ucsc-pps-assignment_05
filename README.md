# UCSC PPS Assignment_05

Imagine the owner of a movie theater who has complete freedom in setting ticket prices. The more he charges, the fewer the people who can afford tickets.

In a recent experiment the owner determined a precise relationship between the price of a ticket and average attendance. At a price of Rs 15.00 per ticket, 120 people attend a performance. Decreasing the price by 5 Rupees increases attendance by 20 and increasing the price by 5 Rupees decreases attendance by 20.

Unfortunately, the increased attendance also comes at an increased cost. Every performance costs the owner Rs.500. Each attendee costs another 3 Rupees.

The owner would like to know the exact relationship between profit and ticket price so that he can determine the price at which he can make the highest profit.

You have to write a C program with multiple functions to solve the above problem.

[_Click here to view the code._](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_05/-/blob/main/PPS_A5_movieTicket.c)
